#!/bin/bash
set -Eeuo pipefail

: ${MEM:=768M}
: ${VNC_PASSWORD:=vncpass}
export VNC_PASSWORD

MODE="${1:-local}"

## first run without slack config we need to launch a browser for login, we need a little extra mem for that
if [ ! -d "$HOME/.config/Slack" ]; then
	mkdir "$HOME/.config/Slack"
	MEM=1024M
fi

case "${MODE}" in
	vnc)
		OPTS="-v $HOME/.config/i3:/home/slack/.config/i3:U -e "VNC_PASSWORD=$VNC_PASSWORD" -p[::1]:5901:5901"
	;;

	local)
		XSOCK=/tmp/.X11-unix/X${DISPLAY##*:}
		XAUTH=/tmp/.podman.xauth
		if [ ! -f "$XAUTH" ]; then
			xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
		fi

		OPTS="-v $XSOCK:$XSOCK:U -v $XAUTH:$XAUTH:U -e DISPLAY=$DISPLAY -e XAUTHORITY=$XAUTH"

	;;

	view)
		vncviewer -AutoSelect=0 -QualityLevel=0 -CompressLevel=9 localhost::5901 &
		exit 0
	;;

	*)
		echo "###########################################################"
		echo "##  syntax:                                              ##"
		echo "##  $0 <vnc|local|view>                                  ##"
		echo "##                                                       ##"
		echo "##  local: will run slack over X11 on local display      ##"
		echo "##  vnc: will run slack as vnc server listening on 5901  ##"
		echo "##  view: just launches vnc viewer focusing on low BW    ##"
		echo "###########################################################"
		exit 1
	;;
esac


podman run --rm --detach --name slack --memory="$MEM" \
	-v $HOME/.config/Slack:/home/slack/.config/Slack:U \
	${OPTS} \
	slack ${MODE}

echo "#############################################"
echo "##  slack should be coming up momentarely  ##"
if [ "${MODE}" == "vnc" ]; then
	echo "##  use 'slack view' to connect to vnc     ##"
fi
echo "#############################################"
