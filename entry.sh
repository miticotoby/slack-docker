#!/bin/bash
set -Eeuo pipefail

## on gentoo / arch and manjaro
## --disable-seccomp-filter-sandbox
## may be needed

: ${SLACK_DEVELOPER_MENU:=true}
export SLACK_DEVELOPER_MENU

: ${SLACK_FULL_SCREEN:=true}
export SLACK_FULL_SCREEN

case $1 in
	"vnc")
		# run local sharable over vnc (vnc password defaults to "vncpass"
		echo "${VNC_PASSWORD:=vncpass}" | vncpasswd -f >$HOME/.vnc/passwd
		vncserver -geometry 1200x800 -localhost no -xstartup /usr/bin/i3
		export DISPLAY=:1
		/usr/bin/slack --no-sandbox --disable-seccomp-filter-sandbox --disable-dev-shm-usage
	;;

	"local")
		# run remote but display local
		#xvfb-run --auto-servernum /usr/bin/slack --disable-gpu --disable-dev-shm-usage --disable-setuid-sandbox --no-sandbox --disable-seccomp-filter-sandbox
		/usr/bin/slack --no-sandbox --disable-seccomp-filter-sandbox --disable-dev-shm-usage
	;;

	"debug")
		/bin/bash
	;;

	*)
		echo "unknown command"
		exit 1
	;;
esac
