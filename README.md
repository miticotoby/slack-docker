# Dockerize slack application

if you refuse (like me) to accept that your chat webUI burns more resources on your box than your compileres but also refuse to have some weird non-trusted app installed as root, but don't wanna waste all your time reverse engeineering code you care very little about this may be for you

in a nutshell this repo includes a Dockerfile and entry.sh to build a container including slack helper script to launch slack as a rootless container limiting mem usage (and ideally eventually also cpu)

2 modes are supported
- independent container (remotely?) and control through VNC
or
- direct display over X11


## quickstart:
1. build a container (`podman build -t slack .`)
2. launch the app (2 options: either local X11 forward or as vnc server)
	- `slack <local|vnc>`
3. the first time you launch you'll have to get it authenticated. this seems to still be a bit buggy, first login will actually drop you in the webui, but close the browser and re-open slack and you should be able to get the desktop app (worst case you may have to copy&paste the magic-auth link


## known issues:
- first login magic token handback from browser to slack app not always working


## also:
- check out the terminal client: https://github.com/miticotoby/slack-term 
