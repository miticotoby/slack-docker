FROM docker.io/library/ubuntu:focal

ENV USR=slack
ENV HOMEDIR=/home/$USR

RUN ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
RUN printf 'XKBMODEL="pc105"\nXKBLAYOUT="us"\nXKBVARIANT=""\nXKBOPTIONS=""\nBACKSPACE="guess"\n' >/etc/default/keyboard
RUN echo 'en_US.UTF-8 UTF-8' >/etc/locale.gen

RUN apt-get update
## install basic deps
RUN LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get -y install locales ca-certificates

COPY slack.list /etc/apt/sources.list.d/
COPY slack-repo.gpg /etc/apt/trusted.gpg.d/

RUN apt-get update
## deps for vnc display
RUN LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get -y install tigervnc-standalone-server i3
## deps for initial login and testing
RUN LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get -y install xterm curl x11-apps telnet vim firefox
## install slack
RUN LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get -y install slack-desktop

RUN rm -r /var/lib/apt/
RUN rm /etc/cron.daily/slack

RUN useradd $USR
RUN mkdir -p $HOMEDIR/.vnc
RUN mkdir -p $HOMEDIR/.config
#RUN printf '#!/bin/sh\n\nxsetroot -solid grey\n#x-terminal-emulator -geometry 80x24+10+10 -ls -title "Slack Desktop" &\n#x-window-manager &\nexport XKL_XMODMAP_DISABLE=1\n/etc/X11/Xsession\n' >$HOMEDIR/.vnc/xstartup
RUN touch $HOMEDIR/.Xauthority
RUN touch $HOMEDIR/.vnc/xstartup
RUN touch $HOMEDIR/.vnc/passwd
RUN ln -s $HOMEDIR/.config/Slack/Downloads $HOMEDIR/Downloads
RUN chmod 600 $HOMEDIR/.vnc/passwd $HOMEDIR/.Xauthority
RUN chmod 700 $HOMEDIR/.vnc/xstartup
RUN chown -R $USR $HOMEDIR

## only exposing this on demand when launched in vnc mode
#EXPOSE 5901
USER $USR
WORKDIR $HOMEDIR
VOLUME /home/slack/.config/i3 /home/slack/.config/Slack

COPY entry.sh /
ENTRYPOINT ["/entry.sh"]
CMD ["local"]
